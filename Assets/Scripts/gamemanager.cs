﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gamemanager : MonoBehaviour {

    public delegate void UpdateBalance();
    public static event UpdateBalance OnUpdateBalance;

    public static gamemanager instance;

    float CurrentBalance;
    string CompanyName;
    // Use this for initialization
    void Start () {
        if (OnUpdateBalance != null)
        {
            OnUpdateBalance();
        }
    }
	
	// Update is called once per frame
	void Update () {
    }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public void AddToBalance(float amt)
    {
        CurrentBalance += amt;
        if (OnUpdateBalance != null)
        {
            OnUpdateBalance();
        }
    }

    public bool CanBuy(float AmtToSpend)
    {
        return !(CurrentBalance < AmtToSpend);
    }

    public float GetCurrentBalance()
    {
        return CurrentBalance;
    }
    public void SetCurrentBalance(float amt)
    {
        CurrentBalance = amt;
    }
    public void SetCompanyName(string name)
    {
        CompanyName = name;
    }
    public string GetCompanyName()
    {
        return CompanyName;
    }
}
