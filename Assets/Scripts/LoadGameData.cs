﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using UnityEngine.UI;

public class LoadGameData : MonoBehaviour {

    public delegate void LoadDataComplete();
    public static event LoadDataComplete OnLoadDataComplete;
    public TextAsset GameData;
    public GameObject StorePrefab;
    public GameObject StorePanel;
    public GameObject ManagerPrefab;
    public GameObject ManagerPanel;

    public void Start()
    {
        LoadData();
        if(OnLoadDataComplete != null)
        {
            OnLoadDataComplete();
        }
    }
    
    public void LoadData()
    {
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(GameData.text);
        XmlNodeList StoreList = xmlDoc.GetElementsByTagName("store");
        XmlNodeList GeneralData = xmlDoc.GetElementsByTagName("GeneralData");
        LoadGeneralData(GeneralData);
        LoadStoreData(StoreList);
        
        
    }
    private void LoadGeneralData(XmlNodeList GeneralDataList)
    {
        foreach (XmlNode Data in GeneralDataList)
        {
            XmlNodeList General = Data.ChildNodes;
            foreach (XmlNode item in General)
            {
                switch (item.Name)
                {
                    case "CurrentBalance":
                        gamemanager.instance.SetCurrentBalance(float.Parse(item.InnerText));
                        break;
                    case "CompanyName":
                        gamemanager.instance.SetCompanyName(item.InnerText);
                        break;
                }
            }
        }
    }
    private void LoadStoreNodes(XmlNodeList NodesToLoad, store thisStore)
    {
        foreach (XmlNode StoreNode in NodesToLoad)
        {
            switch (StoreNode.Name)
            {
                case "BaseStoreProfit":
                    thisStore.BaseStoreProfit = float.Parse(StoreNode.InnerText);
                    break;
                case "BaseStoreCost":
                    thisStore.BaseStoreCost = float.Parse(StoreNode.InnerText);
                    thisStore.SetNextStoreCost();
                    break;
                case "StoreTimer":
                    thisStore.StoreTimer = float.Parse(StoreNode.InnerText);
                    break;
                case "Name":
                    thisStore.name = StoreNode.InnerText;
                    thisStore.transform.Find("StoreNameText").GetComponent<Text>().text = StoreNode.InnerText;
                    thisStore.StoreName = StoreNode.InnerText;
                    break;
                case "Image":
                    thisStore.transform.Find("ImageButtonClick").GetComponent<Image>().sprite = Resources.Load<Sprite>(StoreNode.InnerText);
                    break;
                case "StoreCount":
                    thisStore.StoreCount = int.Parse(StoreNode.InnerText);
                    break;
                case "StoreMultiplier":
                    thisStore.StoreMultiplier = float.Parse(StoreNode.InnerText);
                    break;
                case "StoreUnlocked":
                    thisStore.StoreUnlocked = bool.Parse(StoreNode.InnerText);
                    break;
                case "ManagerCost":
                    CreateManager(StoreNode, thisStore);
                    break;
            }
        }
    }
    private void CreateManager(XmlNode StoreNode, store StoreObj)
    {
        GameObject NewManager = (GameObject)Instantiate(ManagerPrefab);
        NewManager.transform.SetParent(ManagerPanel.transform);
        NewManager.name = StoreObj.StoreName;
        NewManager.transform.Find("ManagerNameText").GetComponent<Text>().text = StoreObj.StoreName;
        StoreObj.ManagerCost = float.Parse(StoreNode.InnerText);
        Button ManagerButton = NewManager.transform.Find("UnlockManagerButton").GetComponent<Button>();
        Text ButtonText = ManagerButton.transform.Find("UnlockManagerButtonText").GetComponent<Text>();
        ButtonText.text = "Unlock " + StoreObj.ManagerCost.ToString("C2");


        UIstore UIManager = StoreObj.transform.GetComponent<UIstore>();
        UIManager.ManagerButton = ManagerButton;
        ManagerButton.onClick.AddListener(StoreObj.UnlockManager);
    }
    private void LoadStoreData(XmlNodeList StoreData)
    {
        foreach (XmlNode StoreInfo in StoreData)
        {

            GameObject NewStore = (GameObject)Instantiate(StorePrefab);
            store storeobj = NewStore.GetComponent<store>();

            XmlNodeList StoreNodes = StoreInfo.ChildNodes;
            LoadStoreNodes(StoreNodes, storeobj);
            NewStore.transform.SetParent(StorePanel.transform);
        }
    }
    
}
