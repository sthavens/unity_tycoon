﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIstore : MonoBehaviour {
    public Text StoreCountText;
    public Slider ProgressSlider;
    public Text BuyButtonText;
    public Button BuyButton;

    public store Store;
    public Button ManagerButton;

    void OnEnable()
    {
        gamemanager.OnUpdateBalance += UpdateUI;
        LoadGameData.OnLoadDataComplete += UpdateUI;
    }

    void OnDisable()
    {
        gamemanager.OnUpdateBalance -= UpdateUI;
        LoadGameData.OnLoadDataComplete -= UpdateUI;
    }

    // Use this for initialization
    public void ManagerUnlocked()
    {
        Text ButtonText = ManagerButton.transform.Find("UnlockManagerButtonText").GetComponent<Text>();
        ButtonText.text = "PURCHASED";
    }
    void Awake()
    {
        Store = transform.GetComponent<store>();
    }
    void Start () {
        StoreCountText.text = Store.StoreCount.ToString();
        
    }
	
	// Update is called once per frame
	void Update () {
        ProgressSlider.value = Store.GetCurrentTimer() / Store.GetStoreTimer();
    }
    public void UpdateUI()
    {
        //Hides panel until store is affordable
        CanvasGroup cg = this.transform.GetComponent<CanvasGroup>();
        if (!Store.StoreUnlocked && !gamemanager.instance.CanBuy(Store.GetNextStoreCost()))
        {
            cg.interactable = false;
            cg.alpha = 0;
        }
        else
        {
            cg.interactable = true;
            cg.alpha = 1;
            Store.StoreUnlocked = true;

        }
        //Shows store if can be purchased
        if (gamemanager.instance.CanBuy(Store.GetNextStoreCost()))
        {
            BuyButton.interactable = true;
        }
        else
        {
            BuyButton.interactable = false;
        }
        BuyButtonText.text = "Buy " + Store.GetNextStoreCost().ToString("C2");

        //Update manager button if can be purchased

        if (!Store.ManagerUnlocked && gamemanager.instance.CanBuy(Store.ManagerCost))
        {
            ManagerButton.interactable = true;
        }
        else
        {
            ManagerButton.interactable = false;
        }
        Text ButtonText = ManagerButton.transform.Find("UnlockManagerButtonText").GetComponent<Text>();
    }
    public void BuyStoreOnClick()
    {
        if (!gamemanager.instance.CanBuy(Store.GetNextStoreCost()))
        {
            return;
        }
        Store.BuyStore();
        StoreCountText.text = Store.StoreCount.ToString();
        UpdateUI();
    }
    public void OnTimerClick()
    {
        Store.OnStartTimer();
    }
}
