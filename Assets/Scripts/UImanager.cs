﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UImanager : MonoBehaviour {

    public enum State
    {
        Main, Managers
    }
    public Text CurrentBalanceText;
    public Text CompanyName;
    public State CurrentState;
    public GameObject ManagerPanel;

    // Use this for initialization
    void Start () {
        OnShowMain();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnShowManagers()
    {
        CurrentState = State.Managers;
        ManagerPanel.SetActive(true);
    }
    void OnShowMain()
    {
        CurrentState = State.Main;
        ManagerPanel.SetActive(false);
    }
    public void OnClickManager()
    {
        if(CurrentState == State.Main)
        {
            OnShowManagers();
        }
        else
        {
            OnShowMain();
        }
    }
    void OnEnable()
    {
        gamemanager.OnUpdateBalance += UpdateUI;
        LoadGameData.OnLoadDataComplete += UpdateUI;
        LoadGameData.OnLoadDataComplete += UpdateName;
    }

    void OnDisable()
    {
        gamemanager.OnUpdateBalance -= UpdateUI;
        LoadGameData.OnLoadDataComplete += UpdateUI;
    }

    public void UpdateUI()
    {
        CurrentBalanceText.text = gamemanager.instance.GetCurrentBalance().ToString("C2");
    }
    public void UpdateName()
    {
        CompanyName.text = gamemanager.instance.GetCompanyName();
    }
}
