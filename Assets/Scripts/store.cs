﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class store : MonoBehaviour {

    //Public Variables - Define Gameplay
    public float BaseStoreCost;
    public float BaseStoreProfit;
    public float StoreTimer;
    public int StoreCount;
    public bool ManagerUnlocked;
    public float StoreMultiplier;
    public bool StoreUnlocked;
    public int StoreTimerDivision = 20;
    public float ManagerCost;
    public string StoreName;
    
    private float NextStoreCost;
    private float CurrentTimer = 0f;
    private bool StartTimer;
    public delegate void ManagerUnlockedDelegate();
    public static event ManagerUnlockedDelegate OnManagerUnlocked;
    
	// Use this for initialization
	void Start () {
        StartTimer = false;
    }
	// Update is called once per frame
	void Update () {
        if (StartTimer)
        {
            CurrentTimer += Time.deltaTime;
            if(CurrentTimer > StoreTimer)
            {
                if (!ManagerUnlocked)
                {
                    StartTimer = false;
                }
                
                CurrentTimer = 0;
                gamemanager.instance.AddToBalance(BaseStoreProfit * StoreCount);
            }
        }
    }
    public void BuyStore()
    {
        StoreCount++;
        float Amt = -NextStoreCost;
        NextStoreCost = (BaseStoreCost * Mathf.Pow(StoreMultiplier, StoreCount));
        gamemanager.instance.AddToBalance(Amt);
        
        if(StoreCount % StoreTimerDivision == 0)
        {
            StoreTimer = StoreTimer / 2;
        }
    }
    public void OnStartTimer()
    {
        
        if (!StartTimer && StoreCount > 0)
        {
            StartTimer = true;
        }
    }
    public float GetCurrentTimer()
    {
        return CurrentTimer;
    }
    public float GetStoreTimer()
    {
        return StoreTimer;
    }
    public float GetNextStoreCost()
    {
        return NextStoreCost;
    }
    public void SetNextStoreCost()
    {
        //BaseStoreCost = amt;
        NextStoreCost = BaseStoreCost;
    }
    public void UnlockManager()
    {
        if (!ManagerUnlocked && gamemanager.instance.CanBuy(ManagerCost))
        {
            gamemanager.instance.AddToBalance(-ManagerCost);
            ManagerUnlocked = true;
            this.transform.GetComponent<UIstore>().ManagerUnlocked();
        }
    }
    
}
